<?php

namespace Drupal\league_oauth_login\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Class used for the event with token and account.
 */
class AccessTokenWithAccountEvent extends Event {

  public function __construct(
    private AccessTokenInterface $accessToken,
    private UserInterface $account,
    private string $providerId,
  ) {}

  /**
   * Getter for token.
   */
  public function getAccessToken(): AccessTokenInterface {
    return $this->accessToken;
  }

  /**
   * Getter for account.
   */
  public function getAccount(): UserInterface {
    return $this->account;
  }

  /**
   * Getter for provider id.
   */
  public function getProviderId(): string {
    return $this->providerId;
  }

}
