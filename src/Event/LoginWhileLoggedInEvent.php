<?php

namespace Drupal\league_oauth_login\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Class used for the login while logged in event.
 */
class LoginWhileLoggedInEvent extends Event {
  use LoginEventTrait;

}
