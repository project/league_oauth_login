<?php

namespace Drupal\Tests\league_oauth_login_github\Kernel;

use Drupal\Tests\league_oauth_login\Kernel\LoginTestBase;

/**
 * Test login.
 *
 * @group league_oauth_login_github
 */
class LoginTest extends LoginTestBase {

  /**
   * {@inheritdoc}
   */
  protected $providerId = 'github';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_github',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getProviderPath() {
    return 'https://github.com/login/oauth/authorize';
  }

}
