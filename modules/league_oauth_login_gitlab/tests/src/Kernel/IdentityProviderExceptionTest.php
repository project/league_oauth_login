<?php

namespace Drupal\Tests\league_oauth_login_gitlab\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\league_oauth_login\Controller\LoginController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginTest.
 *
 * @group league_oauth_login_gitlab
 */
class IdentityProviderExceptionTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'externalauth',
    'league_oauth_login_gitlab',
    'league_oauth_login',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $previous_error_handler = set_error_handler(function ($severity, $message, $file, $line, $context = []) use (&$previous_error_handler) {

      $skipped_deprecations = [
        // @see https://www.drupal.org/project/league_oauth_login/issues/3175293
        'Symfony\Component\EventDispatcher\Event is deprecated in drupal:9.1.0 and will be replaced by Symfony\Contracts\EventDispatcher\Event in drupal:10.0.0. A new Drupal\Component\EventDispatcher\Event class is available to bridge the two versions of the class. See https://www.drupal.org/node/3159012',
      ];

      if (!in_array($message, $skipped_deprecations, TRUE)) {
        return $previous_error_handler($severity, $message, $file, $line, $context);
      }
    }, E_USER_DEPRECATED);
    parent::setUp();
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {
    parent::tearDown();
    restore_error_handler();
  }

  /**
   * Test that the message is displayed if it is possible to see.
   */
  public function testExceptionWithMessage() {
    $request = Request::createFromGlobals();
    $request->query->set('code', 123);
    $state = rand();
    $this->container->get('session')->set('oauth2state', $state);
    $request->query->set('state', $state);
    $controller = LoginController::create($this->container);
    $response = $controller->login($request, 'bogus');
    $response_string = (string) $this->container->get('renderer')->renderRoot($response);
    self::assertEquals($response_string, 'There was a problem logging you in.');
    $messages = $this->container->get('messenger')->all();
    self::assertEquals($messages["error"][0], TestGitlabProvider::ERROR_MESSAGE);
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $def = $container->getDefinition('plugin.manager.league_oauth_login');
    $def->setClass(TestProviderManager::class);
    $container->setDefinition('plugin.manager.league_oauth_login', $def);
  }

}
