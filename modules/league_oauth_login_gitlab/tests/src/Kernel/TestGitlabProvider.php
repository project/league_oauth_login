<?php

namespace Drupal\Tests\league_oauth_login_gitlab\Kernel;

use Drupal\league_oauth_login_gitlab\Plugin\LeagueOauthLogin\GitLab;
use Omines\OAuth2\Client\Provider\Exception\GitlabIdentityProviderException;

/**
 * A provider we use in testing.
 */
class TestGitlabProvider extends GitLab {

  const ERROR_MESSAGE = 'You are not allowed to do that, you must accept terms and what not';

  /**
   * Just a hack to make sure we can trigger this error.
   */
  public function getAccessToken($grant, array $options = []) {
    throw new GitlabIdentityProviderException('Not allowed', 403, '{"message": "' . self::ERROR_MESSAGE . '"}');
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this;
  }

}
