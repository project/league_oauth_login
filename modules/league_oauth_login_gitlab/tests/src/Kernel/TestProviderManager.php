<?php

namespace Drupal\Tests\league_oauth_login_gitlab\Kernel;

use Drupal\league_oauth_login\LeagueOauthLoginPluginManager;

/**
 * A provider manager used in testing.
 */
class TestProviderManager extends LeagueOauthLoginPluginManager {

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return TestGitlabProvider::create(\Drupal::getContainer(), $configuration, $plugin_id, [
      'login_enabled' => TRUE,
    ]);
  }

}
