<?php

namespace Drupal\Tests\league_oauth_login_gitlab\Kernel;

use Drupal\Tests\league_oauth_login\Kernel\LoginTestBase;

/**
 * Test the login.
 *
 * @group league_oauth_login_gitlab
 */
class LoginTest extends LoginTestBase {

  /**
   * {@inheritdoc}
   */
  protected $providerId = 'gitlab';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_gitlab',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getProviderPath() {
    return 'https://gitlab.com/oauth/authorize';
  }

}
