<?php

namespace Drupal\league_oauth_login_test;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericResourceOwner;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Http\Message\ResponseInterface;

/**
 * A not very smart provider.
 *
 * Or very smart... Who knows.
 */
class DummyProvider extends AbstractProvider {

  /**
   * State key we use to control what this finds.
   */
  const STATE_KEY = 'league_dummy_provider';

  /**
   * {@inheritdoc}
   */
  public function getBaseAuthorizationUrl() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseAccessTokenUrl(array $params) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceOwnerDetailsUrl(AccessToken $token) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultScopes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function checkResponse(ResponseInterface $response, $data) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  protected function createResourceOwner(array $response, AccessToken $token) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken($grant, array $options = []) {
    return new AccessToken([
      'access_token' => uniqid('', TRUE),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceOwner(AccessToken $token) {
    $id = \Drupal::state()->get(self::STATE_KEY);
    if (!$id) {
      $id = uniqid('', TRUE);
    }

    return new GenericResourceOwner([
      $id => $id,
      'email' => $id . '@example.com',
    ], $id);
  }

}
