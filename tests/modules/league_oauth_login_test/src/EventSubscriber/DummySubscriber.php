<?php

namespace Drupal\league_oauth_login_test\EventSubscriber;

use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\league_oauth_login\Event\LoginWhileLoggedInEvent;
use Drupal\league_oauth_login\LeagueOauthLoginEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Violinist slack event subscriber.
 */
class DummySubscriber implements EventSubscriberInterface {

  const SPECIAL_STATE_KEY = 'league_oauth_login_test_dummy_enabled';

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  /**
   * Construct a new instance.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * The actual subscriber.
   */
  public function onLoginWithUser(LoginWhileLoggedInEvent $event) {
    $enabled = $this->state->get(self::SPECIAL_STATE_KEY, FALSE);
    if (!$enabled) {
      return;
    }
    $event->setRedirectUrl(Url::fromRoute('system.admin'));
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LeagueOauthLoginEvents::LOGIN_WHILE_LOGGED_IN => ['onLoginWithUser'],
    ];
  }

}
