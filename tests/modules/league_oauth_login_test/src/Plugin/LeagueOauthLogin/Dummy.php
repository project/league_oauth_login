<?php

namespace Drupal\league_oauth_login_test\Plugin\LeagueOauthLogin;

use Drupal\league_oauth_login\LeagueOauthLoginPluginBase;
use Drupal\league_oauth_login_test\DummyProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Example plugin implementation of the league_oauth_login.
 *
 * @LeagueOauthLogin(
 *   id = "dummy",
 *   label = @Translation("Dummy"),
 *   description = @Translation("Dummy login.")
 * )
 */
class Dummy extends LeagueOauthLoginPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getAuthUrlOptions() {
    return [
      'scope' => ['user:email'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return new DummyProvider([
      'redirect_uri' => 'http://localhost',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserName(ResourceOwnerInterface $owner) {
    return uniqid();
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail(ResourceOwnerInterface $owner, $access_token) {
    $owner_data = $owner->toArray();
    if (isset($owner_data['email'])) {
      return $owner_data['email'];
    }
    return sprintf('%s@%s.com', uniqid(), uniqid());
  }

}
