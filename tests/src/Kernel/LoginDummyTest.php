<?php

namespace Drupal\Tests\league_oauth_login\Kernel;

use Drupal\league_oauth_login\Controller\LoginController;
use Drupal\league_oauth_login_test\DummyProvider;
use Drupal\league_oauth_login_test\Plugin\LeagueOauthLogin\Dummy;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Login test with a dummy provider.
 *
 * @group league_oauth_login
 */
class LoginDummyTest extends LoginTestBase {

  /**
   * {@inheritdoc}
   */
  protected $providerId = 'dummy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installSchema('externalauth', ['authmap']);
    $this->installSchema('user', ['users_data']);
  }

  /**
   * {@inheritdoc}
   */
  public function testLoginUserRedirected() {
    self::assertTrue(TRUE, 'This test is totally skipped');
  }

  /**
   * Test that linking works.
   */
  public function testLinking() {
    $request = Request::createFromGlobals();
    $uuid = $this->container->get('uuid');
    $code = $uuid->generate();
    $state = $uuid->generate();
    $request->request->set('code', $code);
    $request->request->set('state', $state);
    /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $session */
    $session = $this->container->get('session');
    $session->set('is_linking', TRUE);
    $session->set('oauth2state', $state);
    /** @var \Drupal\Core\Session\AccountProxyInterface $current_user */
    $current_user = $this->container->get('current_user');
    $account = User::create([
      'name' => 'testuser@example.com',
      'mail' => 'testuser@example.com',
    ]);
    $account->save();
    $current_user->setAccount($account);
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $uuid = $this->container->get('uuid')->generate();
    $state->set(DummyProvider::STATE_KEY, $uuid);
    // So, what we want to do now is make it seem someone else has authed with
    // this authname.
    $account2 = User::create([
      'name' => 'testuser2@example.com',
      'mail' => 'testuser2@example.com',
    ]);
    $account2->save();
    $plugin = Dummy::create($this->container, [], 'dummy', []);
    $authname = LoginController::getAuthName($uuid, $plugin);
    $this->container->get('externalauth.authmap')->save($account2, LoginController::createUserDataKey($plugin), $authname);
    $controller = LoginController::create($this->container);
    $this->expectException(AccessDeniedHttpException::class);
    $this->expectExceptionMessage('User tried to link an existing account');
    $response = $controller->login($request, $this->providerId);
    // There should probably be a better way to detect this, but this is not the
    // error message we want when the authname is already used.
    self::assertNotEquals((string) $response['#markup'], 'There was a problem logging you in.');
  }

  /**
   * Test when we are logging in + registering.
   */
  public function testLoginRegister() {
    /** @var \Drupal\Component\Uuid\Php $uuid */
    $uuid = $this->container->get('uuid');
    $uuid_of_resource_owner = $uuid->generate();
    /** @var \Drupal\externalauth\Authmap $authmap */
    $authmap = $this->container->get('externalauth.authmap');
    $plugin = Dummy::create($this->container, [], 'dummy', []);
    $authname = LoginController::getAuthName($uuid_of_resource_owner, $plugin);
    // So before we do anything, obviously this should not exist.
    $provider_key = LoginController::createUserDataKey($plugin);
    $authname_uid = $authmap->getUid($authname, $provider_key);
    self::assertFalse($authname_uid);
    $request = Request::createFromGlobals();
    $code = $uuid->generate();
    $state = $uuid->generate();
    $request->request->set('code', $code);
    $request->request->set('state', $state);
    /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $session */
    $session = $this->container->get('session');
    $session->set('oauth2state', $state);
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $state->set(DummyProvider::STATE_KEY, $uuid_of_resource_owner);
    $controller = LoginController::create($this->container);
    $controller->login($request, $this->providerId);
    // Now there should be an auth map for this person.
    $authname_uid = $authmap->getUid($authname, $provider_key);
    self::assertNotFalse($authname_uid);
    $user = $this->container->get('entity_type.manager')->getStorage('user')->load($authname_uid);
    self::assertEquals("$uuid_of_resource_owner@example.com", $user->getEmail());
  }

  /**
   * {@inheritdoc}
   */
  protected function getProviderPath() {
    return 'https://github.com/login/oauth/authorize';
  }

}
