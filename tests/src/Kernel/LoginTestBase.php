<?php

namespace Drupal\Tests\league_oauth_login\Kernel;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\league_oauth_login\Controller\LoginController;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Login test base.
 */
abstract class LoginTestBase extends KernelTestBase {

  /**
   * The name of the provider we are trying to use.
   *
   * @var string
   */
  protected $providerId;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'league_oauth_login',
    'externalauth',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('user');
    if (version_compare(\Drupal::VERSION, '10.2', '<')) {
      $this->installSchema('system', 'sequences');
    }

    // Create some config for the provider we are testing.
    $this->createConfigForProvider();
  }

  /**
   * Create config for the provider.
   */
  protected function createConfigForProvider() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->container->get('config.factory')->getEditable('league_oauth_login_' . $this->providerId . '.settings');
    $config->setData([
      'clientId' => rand(),
      'clientSecret' => rand(),
      'redirectUri' => Url::fromRoute('league_oauth_login.login_controller_login', [
        'provider_id' => $this->providerId,
      ], [
        'absolute' => TRUE,
      ])->toString(),
    ]);
    $config->save();
  }

  /**
   * Test that we can log in and get a user created.
   */
  public function testLoginUserRedirected() {
    $request = Request::createFromGlobals();
    $controller = LoginController::create($this->container);
    $response = $controller->login($request, $this->providerId);
    self::assertInstanceOf(TrustedRedirectResponse::class, $response);
    $url = $response->getTargetUrl();
    $parsed = UrlHelper::parse($url);
    self::assertEquals($parsed["query"]["redirect_uri"], Url::fromRoute('league_oauth_login.login_controller_login', [
      'provider_id' => $this->providerId,
    ], [
      'absolute' => TRUE,
    ])->toString());
    self::assertEquals($parsed["path"], $this->getProviderPath());
  }

  /**
   * Test that we can run the login path while logged in.
   */
  public function testLoginWhileLoggedIn() {
    $request = Request::createFromGlobals();
    /** @var \Drupal\Core\Session\AccountProxyInterface $current_user */
    $current_user = $this->container->get('current_user');
    $account = User::create([
      'name' => 'testuser@example.com',
      'mail' => 'testuser@example.com',
    ]);
    $account->save();
    $current_user->setAccount($account);
    $controller = LoginController::create($this->container);
    $response = $controller->login($request, $this->providerId);
    self::assertInstanceOf(TrustedRedirectResponse::class, $response);
    $url = $response->getTargetUrl();
    self::assertEquals($url, '/user');
  }

  /**
   * Test that illegal state will give us access denied.
   */
  public function testIllegalState() {
    $request = Request::createFromGlobals();
    $uuid = $this->container->get('uuid');
    $code = $uuid->generate();
    $state = $uuid->generate();
    $request->request->set('code', $code);
    $request->request->set('state', $state);
    /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $session */
    $session = $this->container->get('session');
    $session->set('oauth2state', "totally not $state");
    $controller = LoginController::create($this->container);
    $this->expectException(AccessDeniedHttpException::class);
    $this->expectExceptionMessage('Illegal state');
    $controller->login($request, $this->providerId);
  }

  /**
   * This should return the expected path when redirected to the provider.
   */
  abstract protected function getProviderPath();

}
