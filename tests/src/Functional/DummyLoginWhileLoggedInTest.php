<?php

namespace Drupal\Tests\league_oauth_login\Kernel;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\league_oauth_login_test\EventSubscriber\DummySubscriber;

/**
 * Functional test to make sure we can override the "logged in redirect".
 */
class DummyLoginWhileLoggedInTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_test',
    'league_oauth_login',
    'externalauth',
    'system',
  ];

  /**
   * Test what happens with a login redirect thing when overridden.
   *
   * Basically, if an event listener wants to override the URL we want to use
   * for the redirect. This should of course not crash the site.
   *
   * @see https://www.drupal.org/project/league_oauth_login/issues/3428697
   */
  public function testLoginRedirect() {
    $user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($user);
    // Now visit the path for the dummy page. We set a special state key so our
    // provider can actually intercept with a new URL.
    $this->container->get('state')->set(DummySubscriber::SPECIAL_STATE_KEY, TRUE);
    $this->drupalGet(Url::fromRoute('league_oauth_login.login_controller_login', ['provider_id' => 'dummy']));
    // The user should of course have no access to the path /admin.
    self::assertEquals(403, $this->getSession()->getStatusCode());
  }

}
